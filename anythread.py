#Author-Dag Rende
#Description-Sketch tool to create a anyThread

# Select a centerpoint min diameter, pitch and the number of turns

# Thanks to Casey Rogers, Patrick Rainsberry, David Liu and Gary Singer - the creators of the Dogbone 
# add-in - for a clean and simple Fusion 360 add-in template.

from collections import defaultdict

import adsk.core, adsk.fusion
import math
import traceback
import re
import os
import random

# pip install pyyaml -t packages; mv packages/yaml .; rm -rf packages
from . import yaml

import time
from . import utils


def run(context):
    try:
        anyThreadCommand.addButton()
    except:
        utils.messageBox(traceback.format_exc())

def stop(context):
    try:
        anyThreadCommand.removeButton()
    except:
        utils.messageBox(traceback.format_exc())
class AnyThreadCommand(object):
    COMMAND_ID = "anyThreadBtn"

    def __init__(self):
        self.app = adsk.core.Application.get()
        self.ui = self.app.userInterface

        self.innerThreadSurface = None
        self.outerThreadSurface = None
        self.pitch = 0
        self.offset = 0
        self.length = 0
        self.gap = 0
        self.rPos = 0

        self.defaults = {
            'pitch': "2 mm",
            'offset': "0 mm",
            'length': "0 mm",
            'gap': "0.3 mm",
            'rPos': "1"
            }

        self.handlers = utils.HandlerHelper()
        self.appPath = os.path.dirname(os.path.abspath(__file__))

    def writeDefaults(self):
        try:
            with open(os.path.join(self.appPath, 'defaults.yml'), 'w') as file:
                documents = yaml.dump(self.defaults, file)
        except:
            pass
    
    def readDefaults(self): 
        if not os.path.isfile(os.path.join(self.appPath, 'defaults.yml')):
            return
        with open(os.path.join(self.appPath, 'defaults.yml'), 'r') as file:
            self.defaults.update(yaml.safe_load(file))


    def addButton(self):
        # clean up any crashed instances of the button if existing
        try:
            self.removeButton()
        except:
            pass

        # add add-in to UI
        button = self.ui.commandDefinitions.addButtonDefinition(
            self.COMMAND_ID, 'AnyThread', 'Creates a threaded fitting', 'Resources')

        button.commandCreated.add(self.handlers.make_handler(adsk.core.CommandCreatedEventHandler,
                                                                    self.onCreate))

        createPanel = self.ui.allToolbarPanels.itemById('SolidCreatePanel')
        buttonControl = createPanel.controls.addCommand(button, self.COMMAND_ID)

        # Make the button available in the panel.
        buttonControl.isPromotedByDefault = True
        buttonControl.isPromoted = True

    def removeButton(self):
        cmdDef = self.ui.commandDefinitions.itemById(self.COMMAND_ID)
        if cmdDef:
            cmdDef.deleteMe()
        createPanel = self.ui.allToolbarPanels.itemById('SolidCreatePanel')
        cntrl = createPanel.controls.itemById(self.COMMAND_ID)
        if cntrl:
            cntrl.deleteMe()

    def onCreate(self, args):
        self.readDefaults()

        inputs = args.command.commandInputs
        args.command.setDialogInitialSize(425, 475)
        args.command.setDialogMinimumSize(425, 475)
        args.command.tooltip = "Select cylindrical surface to thread."


        innerThreadSurface = inputs.addSelectionInput(
            'innerThreadSurface', 'Inner Thread Surface',
            'Select cylindrical surface if inner part to thread')
        innerThreadSurface.addSelectionFilter('CylindricalFaces')
        innerThreadSurface.setSelectionLimits(0,1)

        outerThreadSurface = inputs.addSelectionInput(
            'outerThreadSurface', 'Outer Thread Surface',
            'Select cylindrical surface if outer part to thread')
        outerThreadSurface.addSelectionFilter('CylindricalFaces')
        outerThreadSurface.setSelectionLimits(0,1)

        pitch = inputs.addValueInput(
            'pitch', 'Pitch', self.design.unitsManager.defaultLengthUnits,
            adsk.core.ValueInput.createByString(self.defaults['pitch']))
        pitch.tooltip = "Distance between threads."

        offset = inputs.addValueInput(
            'offset', 'Thread start offset', self.design.unitsManager.defaultLengthUnits,
            adsk.core.ValueInput.createByString(self.defaults['offset']))
        offset.tooltip = "Thread start offset from start of thread surface."

        length = inputs.addValueInput(
            'length', 'Thread length', self.design.unitsManager.defaultLengthUnits,
            adsk.core.ValueInput.createByString(self.defaults['length']))
        length.tooltip = "Thread length from start offset."

        gap = inputs.addValueInput(
            'gap', 'Gap', self.design.unitsManager.defaultLengthUnits,
            adsk.core.ValueInput.createByString(self.defaults['gap']))
        gap.tooltip = "Gap between inner and outer thread."
        
        rPos = inputs.addValueInput(
            'rPos', 'Radial position', self.design.unitsManager.defaultLengthUnits,
            adsk.core.ValueInput.createByString(self.defaults['length']))
        length.tooltip = "Radial thread position (0 into, 1 out from inner surface)."

        # Add handlers to this command.
        args.command.execute.add(self.handlers.make_handler(adsk.core.CommandEventHandler, self.onExecute))
        args.command.validateInputs.add(
            self.handlers.make_handler(adsk.core.ValidateInputsEventHandler, self.onValidate))
        args.command.inputChanged.add(
            self.handlers.make_handler(adsk.core.InputChangedEventHandler, self.onInputChanged))

    def parseInputs(self, inputItems):
        inputs = {inp.id: inp for inp in inputItems}

        self.innerThreadSurface = None
        if inputs['innerThreadSurface'].selectionCount > 0:
            self.innerThreadSurface = inputs['innerThreadSurface'].selection(0).entity
  
        self.outerThreadSurface = None
        if inputs['outerThreadSurface'].selectionCount > 0:
            self.outerThreadSurface = inputs['outerThreadSurface'].selection(0).entity
  
        self.defaults['pitch'] = inputs['pitch'].expression
        self.pitch = inputs['pitch'].value
  
        self.defaults['offset'] = inputs['offset'].expression
        self.offset = inputs['offset'].value
  
        self.defaults['length'] = inputs['length'].expression
        self.length = inputs['length'].value
  
        self.defaults['gap'] = inputs['gap'].expression
        self.gap = inputs['gap'].value

        self.defaults['rPos'] = inputs['rPos'].expression
        self.rPos = inputs['rPos'].value

    def onValidate(self, args):
        cmd = args.firingEvent.sender
        selectedSurfaceCount = 0
        for input in cmd.commandInputs:
            if input.id == 'innerThreadSurface':
                if input.selectionCount == 1:
                    selectedSurfaceCount += 1
            if input.id == 'outerThreadSurface':
                if input.selectionCount == 1:
                    selectedSurfaceCount += 1
        args.areInputsValid = selectedSurfaceCount > 0

    def onInputChanged(self, args):
        cmd = args.firingEvent.sender

    def onExecute(self, args):
        app = adsk.core.Application.get()
        start = time.time()
        doc = app.activeDocument  
        design = app.activeProduct
        timeLine = design.timeline
        timeLineGroups = timeLine.timelineGroups
        timelineCurrentIndex = timeLine.markerPosition
        
        self.parseInputs(args.firingEvent.sender.commandInputs)
        self.writeDefaults()
        self.createThreads()
        
        timelineEndIndex = timeLine.markerPosition
        # if timelineEndIndex - timelineCurrentIndex > 1:
        #     exportTimelineGroup = timeLineGroups.add(timelineCurrentIndex, timelineEndIndex-1)# the minus 1 thing works, weird.
        #     exportTimelineGroup.name = 'Threads'
        


    @property
    def design(self):
        return self.app.activeProduct

    @property
    def rootComp(self):
        return self.design.rootComponent

    def log(self, msg):
        try:
            with open(os.path.join(self.appPath, 'log.txt'), 'a+') as file:
                file.write(msg + "\n")
        except:
            pass

    def createThreads(self):
        if not self.design:
            raise RuntimeError('No active Fusion design')
        if self.innerThreadSurface:
            self.createThread(True)
        if self.outerThreadSurface:
            self.createThread(False)

    def createThread(self, isInner):
        app = adsk.core.Application.get()
        design = app.activeProduct
        otherThreadSurface = self.outerThreadSurface if isInner else self.innerThreadSurface
        threadSurface = self.innerThreadSurface if isInner else self.outerThreadSurface
        threadSurfaceComponent = threadSurface.body.parentComponent
        sketches = threadSurfaceComponent.sketches
        extrudes = threadSurfaceComponent.features.extrudeFeatures
        combineFeatures = threadSurfaceComponent.features.combineFeatures

        if otherThreadSurface:
            otherThreadSurface.body.isVisible = False

        cyl = threadSurface.geometry

        radius = cyl.radius
        axis = cyl.axis
        origin = cyl.origin
        pitch = self.pitch
        length = self.length

        if not isInner:
            radius += self.gap

        # create thread profile
        h = pitch * math.sqrt(3) / 2.0
        skew = 0.001    # overcome sweep tool inconvenience - profle can't be parallel to path even if twisted
        x1 = radius
        y1 = 7 * pitch / 16.0
        z1 = skew * y1
        x2 = radius - 5.0 * h / 8.0
        y2 = pitch / 8.0
        z2 = skew * y2

        profileSketch = sketches.add(threadSurfaceComponent.xYConstructionPlane)
        sketchLines = profileSketch.sketchCurves.sketchLines
        sketchCircles = profileSketch.sketchCurves.sketchCircles

        axis.normalize()
        axisNormal = adsk.core.Vector3D.create(axis.y, -axis.x, 0) if axis.z < axis.x else adsk.core.Vector3D.create(0, -axis.z, axis.y)
        axisNormal.normalize()

        zAxis = axis.crossProduct(axisNormal)
        zAxis.normalize()

        toRadius = axisNormal.copy()
        toRadius.scaleBy(radius)

        toOtherRadius = axisNormal.crossProduct(axis)
        toOtherRadius.normalize()
        toOtherRadius.scaleBy(radius)
        
        toThirdRadius = zAxis.copy()
        toThirdRadius.normalize()
        toThirdRadius.scaleBy(radius)

        # show axes with lines
        axisLine = utils.createLine(profileSketch, origin, utils.scale3d(axis, length + pitch))
        axisLine.isConstruction = True
        axisNormalLine = utils.createLine(profileSketch, origin, axisNormal)
        axisNormalLine.isConstruction = True
        zAxislLine = utils.createLine(profileSketch, origin, zAxis)
        zAxislLine.isConstruction = True

        sweepPath = threadSurfaceComponent.features.createPath(axisLine)

        # create a matrix for a coord system with x=axisNormal, y=axis, z=axisNormalXaxis
        # and transform all coords with it
        tr = adsk.core.Matrix3D.create()
        tr.setWithCoordinateSystem(origin, axisNormal, axis, zAxis)     # origo, x-, y-, z-axis

        p1 = adsk.core.Point3D.create(x1,  y1 - y1,  z1)
        p1.transformBy(tr)
        p2 = adsk.core.Point3D.create(x2,  y2 - y1,  z2)
        p2.transformBy(tr)
        p3 = adsk.core.Point3D.create(x2, -y2 - y1, -z2)
        p3.transformBy(tr)
        p4 = adsk.core.Point3D.create(x1, -y1 - y1, -z1)
        p4.transformBy(tr)

        line1 = sketchLines.addByTwoPoints(
            profileSketch.modelToSketchSpace(p1), 
            profileSketch.modelToSketchSpace(p2))
        line2 = sketchLines.addByTwoPoints(
            line1.endSketchPoint, 
            profileSketch.modelToSketchSpace(p3))
        line3 = sketchLines.addByTwoPoints(
            line2.endSketchPoint, 
            profileSketch.modelToSketchSpace(p4))
        line4 = sketchLines.addByTwoPoints(
            line3.endSketchPoint, 
            line1.startSketchPoint)
        threadProfile = utils.getLastItem(profileSketch.profiles)

        # make a sweep of the shape along sweepPath
        sweeps = threadSurfaceComponent.features.sweepFeatures
        sweepInput = sweeps.createInput(threadProfile, sweepPath, adsk.fusion.FeatureOperations.NewBodyFeatureOperation)
        sweepInput.twistAngle = adsk.core.ValueInput.createByReal(2.0 * math.pi * (length + pitch) / pitch)
        sweep = sweeps.add(sweepInput)
        threadBody = sweep.bodies.item(0)

        threadBody.isVisible = False

        # create cylinder used to cut out the active part of the thread form
        countBefore = profileSketch.profiles.count
        circle = sketchCircles.addByThreePoints(
            profileSketch.modelToSketchSpace(utils.add3d(origin, toRadius)), 
            profileSketch.modelToSketchSpace(utils.add3d(origin, toOtherRadius)), 
            profileSketch.modelToSketchSpace(utils.add3d(origin, toThirdRadius)))
        circleProfiles = adsk.core.ObjectCollection.create()
        while countBefore < profileSketch.profiles.count:
            circleProfiles.add(profileSketch.profiles.item(countBefore))
            countBefore += 1
        distance = adsk.core.ValueInput.createByReal(length)
        boundsCylinder = extrudes.addSimple(circleProfiles, distance, adsk.fusion.FeatureOperations.NewBodyFeatureOperation)        
        boundsCylinderBody = boundsCylinder.bodies.item(0)

        bodyToThread = threadSurface.body
        if not isInner:
            # cut with boundCylinder from outerThreadSurface to make room for threads with gap
            tools = adsk.core.ObjectCollection.create()
            tools.add(boundsCylinderBody)
            combineFeaturesInput = combineFeatures.createInput(bodyToThread, tools)
            combineFeaturesInput.operation = adsk.fusion.FeatureOperations.CutFeatureOperation
            combineFeaturesInput.isKeepToolBodies = True
            cutGapResult = combineFeatures.add(combineFeaturesInput)
            bodyToThread = cutGapResult.bodies.item(0)
            self.log("cutGapResult.bodies.count: " + str(cutGapResult.bodies.count))

        bodyToThread.isVisible = False
        threadSurface.body.isVisible = False
        threadBody.isVisible = True

        # make threads bounded from 0 to length by intersecting with the cylinder
        tools = adsk.core.ObjectCollection.create()
        tools.add(threadBody)
        combineFeaturesInput = combineFeatures.createInput(boundsCylinderBody, tools)
        combineFeaturesInput.operation = adsk.fusion.FeatureOperations.IntersectFeatureOperation
        combineResult = combineFeatures.add(combineFeaturesInput)
        boundedThreadsBody = combineResult.bodies.item(0)
        self.log("combineResult.bodies.count: " + str(combineResult.bodies.count))

        threadSurface.body.isVisible = False
        bodyToThread.isVisible = True

        # cut or add the threads from the original cylnder
        operation = adsk.fusion.FeatureOperations.CutFeatureOperation if isInner else adsk.fusion.FeatureOperations.JoinFeatureOperation
        tools = adsk.core.ObjectCollection.create()
        tools.add(boundedThreadsBody)
        combineFeaturesInput = combineFeatures.createInput(bodyToThread, tools)
        combineFeaturesInput.operation = operation
        combineResult = combineFeatures.add(combineFeaturesInput)
        if otherThreadSurface:
            otherThreadSurface.body.isVisible = True

anyThreadCommand = AnyThreadCommand()
